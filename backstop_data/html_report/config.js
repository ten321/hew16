report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew16_dev_HEWeb_2016_Homepage_0_document_0_phone.png",
        "test": "../bitmaps_test/20220723-082653/backstop_hew16_dev_HEWeb_2016_Homepage_0_document_0_phone.png",
        "selector": "document",
        "fileName": "backstop_hew16_dev_HEWeb_2016_Homepage_0_document_0_phone.png",
        "label": "HEWeb 2016 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://dev-hew16.pantheonsite.io/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew16_dev_HEWeb_2016_Homepage_0_document_1_tablet.png",
        "test": "../bitmaps_test/20220723-082653/backstop_hew16_dev_HEWeb_2016_Homepage_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "backstop_hew16_dev_HEWeb_2016_Homepage_0_document_1_tablet.png",
        "label": "HEWeb 2016 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://dev-hew16.pantheonsite.io/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew16_dev_HEWeb_2016_Homepage_0_document_2_desktop.png",
        "test": "../bitmaps_test/20220723-082653/backstop_hew16_dev_HEWeb_2016_Homepage_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "backstop_hew16_dev_HEWeb_2016_Homepage_0_document_2_desktop.png",
        "label": "HEWeb 2016 Homepage",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://dev-hew16.pantheonsite.io/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew16_dev_HEWeb_2016_Events_0_document_0_phone.png",
        "test": "../bitmaps_test/20220723-082653/backstop_hew16_dev_HEWeb_2016_Events_0_document_0_phone.png",
        "selector": "document",
        "fileName": "backstop_hew16_dev_HEWeb_2016_Events_0_document_0_phone.png",
        "label": "HEWeb 2016 Events",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://dev-hew16.pantheonsite.io/events/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "phone",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew16_dev_HEWeb_2016_Events_0_document_1_tablet.png",
        "test": "../bitmaps_test/20220723-082653/backstop_hew16_dev_HEWeb_2016_Events_0_document_1_tablet.png",
        "selector": "document",
        "fileName": "backstop_hew16_dev_HEWeb_2016_Events_0_document_1_tablet.png",
        "label": "HEWeb 2016 Events",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://dev-hew16.pantheonsite.io/events/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "tablet",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_hew16_dev_HEWeb_2016_Events_0_document_2_desktop.png",
        "test": "../bitmaps_test/20220723-082653/backstop_hew16_dev_HEWeb_2016_Events_0_document_2_desktop.png",
        "selector": "document",
        "fileName": "backstop_hew16_dev_HEWeb_2016_Events_0_document_2_desktop.png",
        "label": "HEWeb 2016 Events",
        "requireSameDimensions": true,
        "misMatchThreshold": 0.1,
        "url": "https://dev-hew16.pantheonsite.io/events/",
        "referenceUrl": "",
        "expect": 0,
        "viewportLabel": "desktop",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    }
  ],
  "id": "backstop_hew16_dev"
});